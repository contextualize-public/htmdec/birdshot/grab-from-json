# grab-from-json

This Python script grabs the value of a property from a JSON file given a property name. It will traverse the nested structure for the property name and return the corresponding value.

## Usage
An example file `ni-srjt-details.json` is included in this package.

Powershell: 
   python grab_from_json.py ni-srjt-details-v2.json "Sample ID"
