import sys
import argparse
import logging
import json
from collections import defaultdict
from pathlib import Path

logger = logging.getLogger(__name__)


class FileNotFoundError(IOError):
    pass

class JsonFlattener:
    @staticmethod
    def flatten_multilevel_json(d, prefix=tuple()):
        """
        Flatten a multi-level JSON dictionary.

        Parameters
        ----------
        d : dict
            JSON-formatted dictionary, possibly multi-level, to be flattened.
        prefix : tuple, optional
            Tuple of keys representing the current path in the JSON hierarchy.

        Returns
        -------
        dict
            Flattened JSON with tuple keys and values.
        """
        if isinstance(d, dict):
            result = {}
            for k, v in d.items():
                if isinstance(v, (dict, list)):
                    result.update(JsonFlattener.flatten_multilevel_json(v, prefix + (k,)))
                else:
                    result[prefix + (k,)] = v
            return result
        elif isinstance(d, list):
            result = defaultdict(list)
            for i, x in enumerate(d):
                for k, v in JsonFlattener.flatten_multilevel_json(x, prefix + (i,)).items():
                    result[k].append(v)
            return result
        else:
            return {prefix: d}
        
class JsonPropertyGrabber:
    """
    A class for grabbing properties from a nested JSON structure.
    """
    @staticmethod
    def find_property(data, property):
        """
        Finds a property in a nested JSON structure.
        
        Parameters
        ----------
        data : dict or list
            The nested JSON data in which to search for the property.
        property : str
            The name of the property to search for.

        Returns
        -------
        list: A list of values for the property found.

        Raises
        ------
        ValueError: If the property is not found.
        """
        logger.debug(f"Finding property {property!r}...")
        for key, value in data.items():
            if property in key:
                return value
        raise ValueError(f"Property {property!r} not found in the flattened JSON data.")

def load(filename: str | Path):
    path = Path(filename)
    if not path.is_file():
        msg = f"{str(path)!r} not found."
        logger.debug(msg)
        raise FileNotFoundError(msg)
    with open(path) as ifs:
        return json.load(ifs)    

def main(params):
    parser = argparse.ArgumentParser(
        description="Find a property in a nested JSON structure")
    parser.add_argument(
        "filename",
        type=str,
        help="Path to the input JSON file.")
    parser.add_argument(
        "property",
        type=str,
        help="Property name to search for.")
    args = parser.parse_args(params)

    data = load(args.filename)
    flattened_data = JsonFlattener.flatten_multilevel_json(data)

  # TODO: This returns all properties matching a name as a flattened list.
    # This means the user can't specify which property (e.g., FILE). If there
    # are multiple FILE fields, they are returned as a (ragged) list that
    # depends on the order in which they were read. This is unstable
    # (stable = same and predictable, unstable = unpredictable), and since all
    # information about where it came from is lost, the user has no way of
    # knowing which FILE they are after.
    #
    # What I propose: refactor to allow users to more precisely identify a
    # property.
    #
    # Usage:
    #
    # grab_from_json data.json [key1 [key2[ ...]]] property
    #
    # Examples:
    #
    # data.json
    # =========
    # {
    #   "foo": {
    #     "bar": [
    #       {"file": <innerA>},
    #       {"file": <innerB>},
    #       {"file": <innerC>}
    #     ]
    #   },
    #   "files": [
    #     {"file": <outerA>},
    #     {"file": <outerB>},
    #     {"file": <outerC>}
    #   ]
    # }
    #
    # [in]$ grab_from_json data.json file
    # <Nothing>
    #
    # [in]$ grab_from_json data.json "*" file
    # [<innerA>, <innerB>, <innerC>, <outerA>, <outerB>, <outerC>]
    #
    # [in]$ grab_from_json data.json foo "*" file
    # [<innerA>, <innerB>, <innerC>]
    #
    # [in]$ grab_from_json data.json foo bar 1 file
    # <innerB>
    #
    # [in]$ grab_from_json data.json foo files "*" file
    # [<outerA>, <outerB>, <outerC>]
    #
    # One option is to include the context information by first flattening the
    # data such that `data.json` becomes:
    #
    # {
    #   ("foo", "bar", 0, "file"): <innerA>,
    #   ("foo", "bar", 1, "file"): <innerB>,
    #   ("foo", "bar", 2, "file"): <innerC>,
    #   ("files", 0, "file"): <outerA>,
    #   ("files", 1, "file"): <outerB>,
    #   ("files", 2, "file"): <outerB>,
    # }
    #
    # then match the user's pattern agains the dictionary keys. This also gives
    # us the option of returning to the user the set of keys that result in
    # each entry.

    try:
        property_value = JsonPropertyGrabber.find_property(flattened_data, args.property)
        logger.info(property_value) # This logs the result
        sys.stdout.write(str(property_value)) # How the user gets the result.
    except ValueError as e:
        logger.info(f"Error: {e}")

def run():
    try:
        return main(sys.argv[1:])
    except Exception as e:
        logger.error(f"An unexpected error occurred: {str(e)}.")
        sys.exit(1)

if __name__ == "__main__":
    run()
